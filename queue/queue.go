package queue

import (
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/otus_golang/calendar/config"
	"log"
)

func GetQueue(c *config.Config) *amqp.Connection {
	conn, err := amqp.Dial(fmt.Sprintf("amqp://%s:%s@%s:%s/",
		c.Queue["user"],
		c.Queue["pass"],
		c.Queue["host"],
		c.Queue["port"],
	))

	if err != nil {
		log.Fatalf("unable to get queue config: %v", err)
	}

	ch, err := conn.Channel()
	if err != nil {
		log.Fatalf("cannot get queue channel: %v", err)
	}
	defer ch.Close()

	err = ch.ExchangeDeclare(
		c.Queue["exchange"], // name
		"fanout",            // type
		true,                // durable
		false,               // auto-deleted
		false,               // internal
		false,               // no-wait
		nil,                 // arguments
	)
	if err != nil {
		log.Fatalf("cannot declare queue exchange: %v", err)
	}

	return conn
}
