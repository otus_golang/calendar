package event

import (
	"context"
	"time"

	"gitlab.com/otus_golang/calendar/models"
)

type Repository interface {
	FindByDate(ctx context.Context, date time.Time) ([]models.Event, error)
}
