package repository

import (
	"context"
	"github.com/jmoiron/sqlx"
	"gitlab.com/otus_golang/calendar/models"
	"log"
	"time"
)

type PsqlEventRepository struct {
	Conn *sqlx.DB
}

func NewPsqlEventRepository(conn *sqlx.DB) *PsqlEventRepository {
	return &PsqlEventRepository{Conn: conn}
}

func (p PsqlEventRepository) FindByDate(ctx context.Context, date time.Time) ([]models.Event, error) {
	query := `
		SELECT id, name, description, date 
		FROM events 
		WHERE date(date) = to_timestamp($1, 'YYYY-MM-DD')
	`

	rows, err := p.Conn.QueryxContext(ctx, query, date)

	if err != nil {
		return nil, err
	}

	defer rows.Close()

	events := make([]models.Event, 0, 1)

	for rows.Next() {
		var event models.Event
		err := rows.StructScan(&event)
		if err != nil {
			log.Fatal(err)
		}
		events = append(events, event)
	}

	return events, nil
}
