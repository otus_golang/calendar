package event

import (
	"gitlab.com/otus_golang/calendar/models"
)

type Queue interface {
	EmitEvent(event *models.Event) error
}
