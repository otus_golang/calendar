package queue

import (
	"encoding/json"
	"fmt"
	"github.com/streadway/amqp"
	"gitlab.com/otus_golang/calendar/config"
	"gitlab.com/otus_golang/calendar/models"
	"go.uber.org/zap"
)

type AmqpEventQueue struct {
	Config *config.Config
	Logger *zap.Logger
	Conn   *amqp.Connection
}

func NewAmqpEventQueue(config *config.Config, logger *zap.Logger, conn *amqp.Connection) *AmqpEventQueue {
	return &AmqpEventQueue{Config: config, Logger: logger, Conn: conn}
}

func (a AmqpEventQueue) EmitEvent(event *models.Event) error {
	ch, err := a.Conn.Channel()
	if err != nil {
		return err
	}
	defer ch.Close()

	body, err := json.Marshal(event)
	if err != nil {
		return err
	}

	err = ch.Publish(
		a.Config.Queue["exchange"], // exchange
		"",                         // routing key
		false,                      // mandatory
		false,                      // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        body,
		})
	if err != nil {
		return err
	}

	a.Logger.Info(fmt.Sprintf(" [x] Sent %s", body))

	return nil
}
