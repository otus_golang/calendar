package event

import (
	"context"

	"gitlab.com/otus_golang/calendar/models"
)

type Repository interface {
	Create(ctx context.Context, event *models.Event) (*models.Event, error)
	Read(ctx context.Context, eventId uint64) (*models.Event, error)
	Update(ctx context.Context, event *models.Event, eventId uint64) (bool, error)
	Delete(ctx context.Context, eventId uint64) (bool, error)
}
