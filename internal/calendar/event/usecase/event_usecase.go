package usecase

import (
	"context"
	"errors"
	"time"

	"gitlab.com/otus_golang/calendar/internal/calendar/event"

	"gitlab.com/otus_golang/calendar/models"
)

type EventUsecase struct {
	eventRepo      event.Repository
	contextTimeout time.Duration
}

func NewEventUsecase(eventRepo event.Repository, contextTimeout time.Duration) *EventUsecase {
	return &EventUsecase{eventRepo: eventRepo, contextTimeout: contextTimeout}
}

func (e *EventUsecase) Create(ctx context.Context, event *models.Event) (*models.Event, error) {
	ctx, cancel := context.WithTimeout(ctx, e.contextTimeout)
	defer cancel()

	newEvent, err := e.eventRepo.Create(ctx, event)
	if err != nil {
		return nil, err
	}
	return newEvent, nil
}

func (e *EventUsecase) Read(ctx context.Context, eventId uint64) (*models.Event, error) {
	ctx, cancel := context.WithTimeout(ctx, e.contextTimeout)
	defer cancel()

	res, err := e.eventRepo.Read(ctx, eventId)
	if err != nil {
		return nil, err
	}
	return res, nil
}

func (e *EventUsecase) Update(ctx context.Context, event *models.Event, eventId uint64) (bool, error) {
	ctx, cancel := context.WithTimeout(ctx, e.contextTimeout)
	defer cancel()

	return e.eventRepo.Update(ctx, event, eventId)
}

func (e *EventUsecase) Delete(ctx context.Context, eventId uint64) (bool, error) {
	ctx, cancel := context.WithTimeout(ctx, e.contextTimeout)
	defer cancel()
	existedEvent, err := e.eventRepo.Read(ctx, eventId)
	if err != nil {
		return false, err
	}
	if existedEvent == nil {
		return false, errors.New("event not found ")
	}
	return e.eventRepo.Delete(ctx, eventId)
}
