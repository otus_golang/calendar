package repository

import (
	"context"
	"fmt"
	"github.com/jmoiron/sqlx"

	"gitlab.com/otus_golang/calendar/models"
)

type PsqlEventRepository struct {
	Conn *sqlx.DB
}

func NewPsqlEventRepository(c *sqlx.DB) *PsqlEventRepository {
	return &PsqlEventRepository{Conn: c}
}

func (p PsqlEventRepository) Create(ctx context.Context, event *models.Event) (*models.Event, error) {
	query := `
		INSERT INTO events(name, description, date)
		VALUES (:name, :description, :date)
		RETURNING id
	`
	stmt, err := p.Conn.PrepareNamedContext(ctx, query)
	if err != nil {
		return nil, err
	}

	defer stmt.Close()

	var id uint32
	err = stmt.Get(&id, map[string]interface{}{
		"name":        event.Name,
		"description": event.Description,
		"date":        event.Date,
	})
	if err != nil {
		return nil, err
	}

	event.Id = id
	return event, nil
}

func (p PsqlEventRepository) Read(ctx context.Context, eventId uint64) (*models.Event, error) {
	query := `SELECT id, name, description, date FROM events WHERE ID = $1`

	row := p.Conn.QueryRowContext(ctx, query, eventId)

	event := models.Event{}
	err := row.Scan(&event.Id, &event.Name, &event.Description, &event.Date)

	if err != nil {
		return nil, err
	}

	return &event, nil
}

func (p PsqlEventRepository) Update(ctx context.Context, event *models.Event, eventId uint64) (bool, error) {
	query := `UPDATE events set name=$1, description=$2, date=$3 WHERE ID = $4`

	stmt, err := p.Conn.PrepareContext(ctx, query)
	if err != nil {
		return false, nil
	}

	defer stmt.Close()

	res, err := stmt.ExecContext(ctx, event.Name, event.Description, event.Date, eventId)
	if err != nil {
		return false, err
	}
	affect, err := res.RowsAffected()
	if err != nil {
		return false, err
	}
	if affect != 1 {
		err = fmt.Errorf("Weird  Behaviour. Total Affected: %d", affect)

		return false, err
	}

	return true, nil
}

func (p PsqlEventRepository) Delete(ctx context.Context, eventId uint64) (bool, error) {
	query := "DELETE FROM events WHERE id = $1"

	stmt, err := p.Conn.PrepareContext(ctx, query)
	if err != nil {
		return false, err
	}

	defer stmt.Close()

	res, err := stmt.ExecContext(ctx, eventId)
	if err != nil {
		return false, err
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return false, err
	}

	if rowsAffected != 1 {
		err = fmt.Errorf("Weird  Behaviour. Total Affected: %d", rowsAffected)
		return false, err
	}

	return true, nil
}
