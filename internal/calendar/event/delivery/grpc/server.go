package grpc

import (
	"context"
	"go.uber.org/zap"
	"time"

	"gitlab.com/otus_golang/calendar/internal/calendar/event/delivery/grpc/api"

	"github.com/golang/protobuf/ptypes"
	"gitlab.com/otus_golang/calendar/internal/calendar/event/usecase"
	"gitlab.com/otus_golang/calendar/models"
)

type Server struct {
	*usecase.EventUsecase
	*zap.Logger
}

func NewServer(eventUsecase *usecase.EventUsecase, logger *zap.Logger) *Server {
	return &Server{EventUsecase: eventUsecase, Logger: logger}
}

func (s *Server) Create(ctx context.Context, req *apipb.CreateRequest) (*apipb.CreateResponse, error) {
	success := false
	e := s.unmarshalPbEvent(req.Event)
	resEvent, err := s.EventUsecase.Create(ctx, &e)

	if err != nil {
		s.Logger.Error(err.Error())
		return nil, err
	} else {
		success = true
	}

	res := apipb.CreateResponse{
		Id:      resEvent.Id,
		Success: success,
	}

	return &res, nil
}

func (s *Server) Read(ctx context.Context, req *apipb.ReadRequest) (*apipb.ReadResponse, error) {
	event, err := s.EventUsecase.Read(ctx, uint64(req.Id))

	if err != nil {
		s.Logger.Error(err.Error())
		return nil, err
	}

	res := apipb.ReadResponse{
		Event: s.marshalEvent(event),
	}

	return &res, nil

}

func (s *Server) Update(ctx context.Context, req *apipb.UpdateRequest) (*apipb.UpdateResponse, error) {
	event := s.unmarshalPbEvent(req.Event)
	success, err := s.EventUsecase.Update(ctx, &event, uint64(req.Id))

	if err != nil {
		s.Logger.Error(err.Error())
		return nil, err
	}

	return &apipb.UpdateResponse{
		Success: success,
	}, nil
}

func (s *Server) Delete(ctx context.Context, req *apipb.DeleteRequest) (*apipb.DeleteResponse, error) {
	success, err := s.EventUsecase.Delete(ctx, uint64(req.GetId()))
	if err != nil {
		s.Logger.Error(err.Error())
		return nil, err
	}

	return &apipb.DeleteResponse{
		Success: success,
	}, nil
}

func (s *Server) unmarshalPbEvent(e *apipb.Event) models.Event {
	eventStruct := models.Event{
		Id:          e.GetId(),
		Name:        e.GetName(),
		Description: e.GetDescription(),
		Date:        ptypes.TimestampString(e.GetDate()),
	}
	return eventStruct
}

func (s *Server) marshalEvent(e *models.Event) *apipb.Event {
	t, err := time.Parse(time.RFC3339, e.Date)
	if err != nil {
		s.Logger.Error(err.Error())
	}

	ts, err := ptypes.TimestampProto(t)
	if err != nil {
		s.Logger.Error(err.Error())
	}
	pbEvent := apipb.Event{
		Id:          e.Id,
		Name:        e.Name,
		Description: e.Description,
		Date:        ts,
	}
	return &pbEvent
}
