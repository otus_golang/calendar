package grpc

import (
	"context"
	apipb "gitlab.com/otus_golang/calendar/internal/calendar/event/delivery/grpc/api"
	"google.golang.org/grpc"
)

type Client struct {
}

func (c Client) Create(ctx context.Context, in *apipb.CreateRequest, opts ...grpc.CallOption) (*apipb.CreateResponse, error) {
	panic("implement me")
}

func (c Client) Read(ctx context.Context, in *apipb.ReadRequest, opts ...grpc.CallOption) (*apipb.ReadResponse, error) {
	panic("implement me")
}

func (c Client) Update(ctx context.Context, in *apipb.UpdateRequest, opts ...grpc.CallOption) (*apipb.UpdateResponse, error) {
	panic("implement me")
}

func (c Client) Delete(ctx context.Context, in *apipb.DeleteRequest, opts ...grpc.CallOption) (*apipb.DeleteResponse, error) {
	panic("implement me")
}
