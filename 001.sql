create table events
(
    id          serial not null
        constraint events_pk
            primary key,
    name        text   not null,
    description text,
    date        timestamp
);
