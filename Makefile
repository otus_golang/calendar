.PHONY: up down restart test fmt vet lint tidy verify imports precommit gen-proto docker_build_calendar docker_build_scanner docker_build_notifier

DOCKER_COMPOSE_FILE ?= deployments/docker-compose/docker-compose.yml
DOCKER_COMPOSE_TEST_FILE ?= deployments/docker-compose/docker-compose.test.yml

fmt:
	gofmt -w -s -d .
vet:
	go vet .
lint:
	golint .
tidy:
	go mod tidy
verify:
	go mod verify
imports:
	goimports -w .
precommit: fmt vet lint tidy verify imports
gen-proto:
	 protoc -I/usr/local/include -I. api/event.proto --go_out=plugins=grpc:internal/calendar/event/delivery/grpc

up:
	docker-compose -f ${DOCKER_COMPOSE_FILE} up -d

down:
	docker-compose -f ${DOCKER_COMPOSE_FILE} down

restart: down up

test:
	docker-compose -f ${DOCKER_COMPOSE_TEST_FILE} up --build -d ;\
    docker-compose -f ${DOCKER_COMPOSE_TEST_FILE} run integration_tests go test ./tests/integration;\
    test_status_code=$$? ;\
    docker-compose -f ${DOCKER_COMPOSE_TEST_FILE} down ;\
    exit $$test_status_code ;\

run_integration_tests:


docker_build_calendar:
	docker build -t calendar -f deployments/docker/calendar/Dockerfile .

docker_build_scanner:
	docker build -t scanner -f deployments/docker/scanner/Dockerfile .

docker_build_notifier:
	docker build -t notifier -f deployments/docker/notifier/Dockerfile .
