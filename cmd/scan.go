package cmd

import (
	"context"
	"github.com/spf13/cobra"
	"gitlab.com/otus_golang/calendar/config"
	db2 "gitlab.com/otus_golang/calendar/db"
	"gitlab.com/otus_golang/calendar/internal/scanner/event/queue"
	"gitlab.com/otus_golang/calendar/internal/scanner/event/repository"
	"gitlab.com/otus_golang/calendar/internal/scanner/event/usecase"
	"gitlab.com/otus_golang/calendar/logger"
	queue2 "gitlab.com/otus_golang/calendar/queue"
	"log"
	"time"
)

func init() {
	rootCmd.AddCommand(scan)
}

var scan = &cobra.Command{
	Use:   "scan",
	Short: "Start scanner server",
	Long:  `Start grpc scanner server`,
	Run: func(cmd *cobra.Command, args []string) {
		c, err := config.GetConfig()
		if err != nil {
			log.Fatal("failed get config")
		}

		l, err := logger.GetLogger(c)
		if err != nil {
			log.Fatal("failed get logger")
		}

		db, err := db2.GetDb(c)
		if err != nil {
			l.Fatal("failed get db")
		}

		amqp := queue2.GetQueue(c)
		defer amqp.Close()

		var contextTimeout time.Duration
		if c.IsDevelopment() {
			contextTimeout = time.Hour
		} else {
			contextTimeout = time.Millisecond * 500
		}

		r := repository.NewPsqlEventRepository(db)
		q := queue.NewAmqpEventQueue(c, l, amqp)
		u := usecase.NewEventUsecase(r, q, contextTimeout)
		ctx := context.Background()

		t := time.Now()
		n := time.Date(t.Year(), t.Month(), t.Day(), 12, 0, 0, 0, t.Location())
		d := n.Sub(t)
		if d < 0 {
			n = n.Add(24 * time.Hour)
			d = n.Sub(t)
		}
		for {
			err := u.EmitEvent(ctx, time.Now())
			if err != nil {
				l.Error(err.Error())
			}

			time.Sleep(d)
		}

	},
}
