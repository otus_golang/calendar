package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.com/otus_golang/calendar/config"
	"gitlab.com/otus_golang/calendar/logger"
	"gitlab.com/otus_golang/calendar/queue"
	"go.uber.org/zap"
	"log"
)

func init() {
	rootCmd.AddCommand(notifier)
}

var notifier = &cobra.Command{
	Use:   "notifier",
	Short: "notifier service",
	Long:  `notifier service`,
	Run: func(cmd *cobra.Command, args []string) {
		c, err := config.GetConfig()
		if err != nil {
			log.Fatalf("unable to load config: %v", err)
		}

		l, err := logger.GetLogger(c)
		if err != nil {
			log.Fatalf("unable to load logger: %v", err)
		}

		amqpQueue := queue.GetQueue(c)
		defer amqpQueue.Close()

		ch, err := amqpQueue.Channel()
		failOnError(l, err, "Failed to open a channel")
		defer ch.Close()

		err = ch.ExchangeDeclare(
			c.Queue["exchange"], // name
			"fanout",            // type
			true,                // durable
			false,               // auto-deleted
			false,               // internal
			false,               // no-wait
			nil,                 // arguments
		)
		failOnError(l, err, "Failed to declare an exchange")

		q, err := ch.QueueDeclare(
			c.Queue["queue"], // name
			true,             // durable
			false,            // delete when unused
			false,            // exclusive
			false,            // no-wait
			nil,              // arguments
		)
		failOnError(l, err, "Failed to declare a queue")

		err = ch.QueueBind(
			q.Name,     // queue name
			"",         // routing key
			"calendar", // exchange
			false,
			nil)
		failOnError(l, err, "Failed to bind a queue")

		msgs, err := ch.Consume(
			q.Name, // queue
			"",     // consumer
			true,   // auto-ack
			false,  // exclusive
			false,  // no-local
			false,  // no-wait
			nil,    // args
		)
		failOnError(l, err, "Failed to register a consumer")

		forever := make(chan bool)

		go func() {
			for d := range msgs {
				log.Printf(" [x] %s", d.Body)
			}
		}()

		log.Printf(" [*] Waiting for logs. To exit press CTRL+C")
		<-forever
	},
}

func failOnError(l *zap.Logger, e error, s string) {
	if e != nil {
		l.Fatal(fmt.Sprintf("%s: %v", s, e))
	}
}
