package cmd

import (
	"fmt"
	grpc_prometheus "github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/otus_golang/calendar/config"
	db2 "gitlab.com/otus_golang/calendar/db"
	deliveryGrpc "gitlab.com/otus_golang/calendar/internal/calendar/event/delivery/grpc"
	apipb "gitlab.com/otus_golang/calendar/internal/calendar/event/delivery/grpc/api"
	"gitlab.com/otus_golang/calendar/internal/calendar/event/repository"
	"gitlab.com/otus_golang/calendar/internal/calendar/event/usecase"
	"gitlab.com/otus_golang/calendar/logger"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"log"
	"net"
	"net/http"
	"time"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(serve)
}

var serve = &cobra.Command{
	Use:   "serve",
	Short: "Start calendar server",
	Long:  `Start grpc calendar server`,
	Run: func(cmd *cobra.Command, args []string) {
		c, err := config.GetConfig()
		if err != nil {
			log.Fatalf("unable to load config: %v", err)
		}

		l, err := logger.GetLogger(c)
		if err != nil {
			log.Fatalf("unable to load logger: %v", err)
		}

		db, err := db2.GetDb(c)

		if err != nil {
			log.Fatalf("unable to load db: %v", err)
		}

		lis, err := net.Listen("tcp", c.Host+":"+c.Port)
		if err != nil {
			l.Fatal(fmt.Sprintf("failed to listen %v", err))
		}
		grpcServer := grpc.NewServer(
			grpc.StreamInterceptor(grpc_prometheus.StreamServerInterceptor),
			grpc.UnaryInterceptor(grpc_prometheus.UnaryServerInterceptor),
		)

		var contextTimeout time.Duration
		if c.IsDevelopment() {
			reflection.Register(grpcServer)
			contextTimeout = time.Hour
		} else {
			contextTimeout = time.Millisecond * 500
		}

		r := repository.NewPsqlEventRepository(db)
		u := usecase.NewEventUsecase(r, contextTimeout)
		apipb.RegisterEventServiceServer(grpcServer, deliveryGrpc.NewServer(u, l))

		grpc_prometheus.Register(grpcServer)
		grpc_prometheus.EnableHandlingTimeHistogram()
		l.Info(fmt.Sprintf("Monitoring export listen %s", c.Prometheus["host"]))
		err = http.ListenAndServe(c.Prometheus["host"], promhttp.Handler())
		if err != nil {
			l.Error(err.Error())
		}
		http.Handle("/metrics", promhttp.Handler())

		err = grpcServer.Serve(lis)
		if err != nil {
			l.Fatal(err.Error())
		}
	},
}
