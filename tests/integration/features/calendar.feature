# file: calendar.feature
Feature: calendar
  In order to create event
  As an GRPC client
  I need to be able to send event

  Scenario: should save event
    When I call grpc calendar method Create
    Then The error should be nil
    And The create response success should be true

  Scenario: should update event
    When I call grpc calendar method Update
    Then The error should be nil
    And The update response success should be true
