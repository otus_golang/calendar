package integration

import (
	"context"
	"fmt"
	"github.com/DATA-DOG/godog"
	"github.com/golang/protobuf/ptypes/timestamp"
	apipb "gitlab.com/otus_golang/calendar/internal/calendar/event/delivery/grpc/api"
	"google.golang.org/grpc"
	"os"
	"time"
)

var grpcService = os.Getenv("GRPC_SERVICE")
var createdId uint32
var createResponse *apipb.CreateResponse
var updateResponse *apipb.UpdateResponse
var responseErr error

func init() {
	if grpcService == "" {
		grpcService = "localhost:50051"
	}
}

func iCallGrpcCalendarMethod() error {
	cc, err := grpc.Dial(grpcService, grpc.WithInsecure())
	if err != nil {
		return fmt.Errorf("could not connect: %v", err)
	}
	defer cc.Close()

	c := apipb.NewEventServiceClient(cc)
	ctx, cancel := context.WithTimeout(context.Background(), 400*time.Millisecond)
	defer cancel()

	in := apipb.CreateRequest{
		Event: &apipb.Event{
			Name:        "Test",
			Description: "Behavior testing",
			Date: &timestamp.Timestamp{
				Seconds: time.Now().Unix(),
			},
		},
	}

	createResponse, responseErr = c.Create(ctx, &in)

	return nil
}

func theErrorShouldBeNil() error {
	return responseErr
}

func theCreateResponseSuccessShouldBeTrue() error {
	if !createResponse.Success {
		return fmt.Errorf("createResponse success is false")
	}
	createdId = createResponse.Id
	return nil
}

func iCallGrpcCalendarMethodUpdate() error {
	cc, err := grpc.Dial(grpcService, grpc.WithInsecure())
	if err != nil {
		return fmt.Errorf("could not connect: %v", err)
	}
	defer cc.Close()

	c := apipb.NewEventServiceClient(cc)
	ctx, cancel := context.WithTimeout(context.Background(), 400*time.Millisecond)
	defer cancel()

	in := apipb.UpdateRequest{
		Id: createdId,
		Event: &apipb.Event{
			Name:        "Test update",
			Description: "Behavior testing",
			Date: &timestamp.Timestamp{
				Seconds: time.Now().Unix(),
			},
		},
	}

	updateResponse, responseErr = c.Update(ctx, &in)

	return nil
}

func theUpdateResponseSuccessShouldBeTrue() error {
	if !updateResponse.Success {
		return fmt.Errorf("createResponse success is false")
	}
	return nil
}

func FeatureContext(s *godog.Suite) {
	s.Step(`^I call grpc calendar method Create$`, iCallGrpcCalendarMethod)
	s.Step(`^The error should be nil$`, theErrorShouldBeNil)
	s.Step(`^The create response success should be true$`, theCreateResponseSuccessShouldBeTrue)

	s.Step(`^I call grpc calendar method Update$`, iCallGrpcCalendarMethodUpdate)
	s.Step(`^The error should be nil$`, theErrorShouldBeNil)
	s.Step(`^The update response success should be true$`, theUpdateResponseSuccessShouldBeTrue)
}
